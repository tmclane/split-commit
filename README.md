# Assignment

Given this state of the repository:

    * 7744b6d (master) add missing return value
    * 30554ec implemented basic argument parsing
    * 264640c initial commit

Your assignment is to do the following:

- Check out a working branch from the `master` branch named `feature-print-args`.
- Split the whitespace changes out from the functional changes in the first commit on your branch.
  You should end up with two commits on the branch.
- Ensure the 'add missing return value' commit is included on the feature branch commit loop


When you are done, it should look like this:

    * 7744b6d (master) add missing return value
    * 30554ec implemented basic argument parsing
    | *   e01efa1 (feature-print-args) Merge branch 'feature-print-args' into master
    | |\
    |/ /
    | * 25a1ea2 add missing return value
    | * a4ad8ca implement basic argument processing
    | * 6d82272 noop: whitespace change
    |/
    * 264640c initial commit
